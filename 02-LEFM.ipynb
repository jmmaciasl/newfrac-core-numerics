{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "dress-paradise",
   "metadata": {},
   "source": [
    "# Linear Elasticity Fracture Mechanics\n",
    "\n",
    "*Authors: Laura De Lorenzis (ETH Zürich) and Corrado Maurini (corrado.maurini@sorbonne-universite.fr)*\n",
    "\n",
    "This notebook serves as a tutorial for linear elastic fracture mechanics\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "detected-ordinary",
   "metadata": {},
   "outputs": [],
   "source": [
    "%config Completer.use_jedi = False\n",
    "%config IPCompleter.greedy = True\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "import dolfinx\n",
    "import ufl\n",
    "\n",
    "from mpi4py import MPI\n",
    "from petsc4py import PETSc\n",
    "\n",
    "import elastic_solver\n",
    "\n",
    "from utils import project\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "increasing-upset",
   "metadata": {},
   "source": [
    "# Asymptotic field and SIF ($K_I$)\n",
    "\n",
    "Let us first get the elastic solution for a given crack length "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "joined-planning",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "WARNING:py.warnings:/usr/local/lib/python3.8/dist-packages/numpy/ctypeslib.py:521: RuntimeWarning: A builtin ctypes object gave a PEP3118 format string that does not match its itemsize, so a best-guess will be made of the data type. Newer versions of python may behave correctly.\n",
      "  return array(obj, copy=False)\n",
      "\n"
     ]
    }
   ],
   "source": [
    "Lcrack = 0.3\n",
    "Lx = 1.\n",
    "uh, energy = elastic_solver.solve_elasticity(Lx=Lx,Ly=.5,Lcrack=Lcrack,lc=.05,refinement_ratio=30,dist_min=.2,dist_max=.3)\n",
    "\n",
    "plt.colorbar(dolfinx.plotting.plot(uh,mode=\"displacement\"),orientation=\"horizontal\")\n",
    "plt.title('Elastic solver - displacement field')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "friendly-wheel",
   "metadata": {},
   "source": [
    "## Crack opening displacement (COD)\n",
    "\n",
    "Let us get the vertical displacement at the crack lip"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "handled-sigma",
   "metadata": {},
   "outputs": [],
   "source": [
    "mesh = uh.function_space.mesh\n",
    "bottom_nodes = np.where(mesh.geometry.x[:,1] == 0)\n",
    "xs = mesh.geometry.x[bottom_nodes,0][0]\n",
    "us = uh.compute_point_values()[bottom_nodes,1][0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "labeled-monthly",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(xs,us,\".\")\n",
    "plt.xlim([0.,1.1*Lcrack])\n",
    "plt.xlabel(\"x - coordinate\")\n",
    "plt.ylabel(r\"$u_y$\")\n",
    "plt.title(\"Crack opening displacement\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "blank-notion",
   "metadata": {},
   "source": [
    "As detailed in the lectures notes, we can estimate the value of the stress intensity factor $K_I$ by extrapolating $u \\sqrt{2\\pi/ r}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "geological-prefix",
   "metadata": {},
   "outputs": [],
   "source": [
    "r = (Lcrack-xs)\n",
    "r_ = r[np.where(xs<Lcrack)]\n",
    "us_ = us[np.where(xs<Lcrack)]\n",
    "\n",
    "kappa = (3 - elastic_solver.nu) / (1 + elastic_solver.nu)\n",
    "factor = 2 * elastic_solver.mu / (kappa + 1)\n",
    "\n",
    "plt.semilogx(r,us*np.sqrt(2*np.pi/r)*factor,\".\")\n",
    "plt.xlabel(\"r\")\n",
    "plt.ylabel(r\"${u_y} \\,\\frac{2\\mu}{k+1} \\,\\sqrt{2\\pi/r}$\")\n",
    "plt.title(\"Crack opening displacement\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "level-alignment",
   "metadata": {},
   "source": [
    "We estimate $K_I\\simeq 0.18$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "happy-classic",
   "metadata": {},
   "source": [
    "## Stress at the crack tip\n",
    "\n",
    "Let us get the stress around the crack tip"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dental-intention",
   "metadata": {},
   "outputs": [],
   "source": [
    "sigmah = elastic_solver.sigma(elastic_solver.eps(uh))\n",
    "stress = dolfinx.Function(dolfinx.TensorFunctionSpace(mesh, ('DG', 0)), name=\"S\")\n",
    "project(sigmah,stress)\n",
    "plt.colorbar(dolfinx.plotting.plot(stress.sub(1),vmax=.3))\n",
    "plt.title(r\"$\\sigma_{xx}$\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "pleasant-frank",
   "metadata": {},
   "outputs": [],
   "source": [
    "r = (xs-Lcrack)\n",
    "stress_xx_bottom = stress.compute_point_values()[bottom_nodes,0][0]\n",
    "r_ = r[np.where(xs>Lcrack)]\n",
    "stress_xx = stress_xx_bottom[np.where(xs>Lcrack)]\n",
    "plt.plot(r_,stress_xx,\"o\")\n",
    "plt.xlabel(\"r\")\n",
    "plt.ylabel(r\"$\\sigma_{rr}$\")\n",
    "plt.title(\"Stress at the crack tip\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "whole-dakota",
   "metadata": {},
   "source": [
    "As detailed in the lectures notes, we can estimate the value of the stress intensity factor $K_I$ by extrapolating $\\sigma_{rr} \\sqrt{2\\pi r}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "imperial-senegal",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.semilogx(r_,stress_xx*np.sqrt(2*np.pi*r_),\"o\")\n",
    "plt.xlabel(\"r\")\n",
    "plt.ylabel(r\"$\\sigma_{rr}*\\sqrt{2\\pi\\,r}$\")\n",
    "plt.title(\"Stress at the crack tip\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "directed-jones",
   "metadata": {},
   "source": [
    "We can say that $K_I\\simeq 0.18$ as from the COD, but this estimate is not precise and reliable. \n",
    "\n",
    "From Irwing formula in plane-stress, we get the energy release rate (ERR)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "posted-makeup",
   "metadata": {},
   "outputs": [],
   "source": [
    "KI_estimate = 0.18 #+-.02\n",
    "G_estimate = KI_estimate ** 2 / elastic_solver.E # Irwin's formula in plane stress\n",
    "print(f\"ERR estimate is {G_estimate}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "specific-cassette",
   "metadata": {},
   "source": [
    "# The elastic energy release rate "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "musical-steering",
   "metadata": {},
   "source": [
    "## Naïf method: finite difference of the potential energy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "surface-pierce",
   "metadata": {},
   "source": [
    "Let us first calculate the potential energy for several crack lengths. We multiply the result by 2 to account for the symmetry when comparing with the $K_I$ estimate above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "sealed-ukraine",
   "metadata": {},
   "outputs": [],
   "source": [
    "Ls = np.linspace(Lcrack*.7,Lcrack*1.3,10)\n",
    "energies = np.zeros_like(Ls)\n",
    "Gs = np.zeros_like(Ls)\n",
    "for (i, L) in enumerate(Ls):\n",
    "    uh, energies[i] = elastic_solver.solve_elasticity(Lx=1,Ly=.5,Lcrack=L,lc=.1,refinement_ratio=10,dist_min=.2,dist_max=.3)\n",
    "    \n",
    "energies = energies * 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "incorporate-soviet",
   "metadata": {},
   "source": [
    "We can estimate the ERR by taking the finite-difference approximation of the derivative"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "metropolitan-tokyo",
   "metadata": {},
   "outputs": [],
   "source": [
    "energies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "orange-purpose",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.diff(energies)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "invalid-probe",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.diff(Ls)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "transsexual-leadership",
   "metadata": {},
   "outputs": [],
   "source": [
    "# ERRnaif = P(Li)-P(Lim1)/(Li-Lim1)\n",
    "ERR_naif = -np.diff(energies)/np.diff(Ls)\n",
    "ERR_naif"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "frank-moderator",
   "metadata": {},
   "outputs": [],
   "source": [
    "ERR_naif = -np.diff(energies)/np.diff(Ls)\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(Ls, energies,\"*\")\n",
    "plt.xlabel(\"L_crack\")\n",
    "plt.ylabel(\"Potential energy\")\n",
    "plt.figure()\n",
    "plt.plot(Ls[0:-1], ERR_naif,\"o\")\n",
    "plt.ylabel(\"ERR\")\n",
    "plt.xlabel(\"L_crack\")\n",
    "plt.axhline(G_estimate,linestyle='--',color=\"gray\")\n",
    "plt.axvline(Lcrack,linestyle='--',color=\"gray\") "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "rocky-interstate",
   "metadata": {},
   "source": [
    "## G-theta method: domain derivative\n",
    "\n",
    "This function implement the G-theta method to compte the ERR as described in the lecture notes.\n",
    "\n",
    "We first create by an auxiliary computation a suitable theta-field."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "every-battery",
   "metadata": {},
   "outputs": [],
   "source": [
    "def create_theta_field(mesh,Lcrack,R_int=.1):\n",
    "    \n",
    "    element = ufl.FiniteElement('Lagrange',mesh.ufl_cell(),degree=1)   \n",
    "    V0 = dolfinx.FunctionSpace(mesh, element)\n",
    "    \n",
    "    one = dolfinx.Function(V0)\n",
    "    with one.vector.localForm() as one_loc:\n",
    "        one_loc.set(1)\n",
    "    \n",
    "    zero = dolfinx.Function(V0)\n",
    "    with zero.vector.localForm() as zero_loc:\n",
    "        zero_loc.set(0)\n",
    "    \n",
    "    def inner_circle(x):\n",
    "        return np.sqrt((x[0]-Lcrack)**2 + x[1]**2) < R_int \n",
    "    \n",
    "    # Impose theta to 1 close to the tip\n",
    "    facets = dolfinx.mesh.locate_entities(mesh, 1, inner_circle)\n",
    "    dofs_inner = dolfinx.fem.locate_dofs_topological(V0, 1, facets)\n",
    "    bc_inner = dolfinx.DirichletBC(one,dofs_inner)\n",
    "    \n",
    "    # Impose theta to vanish on the boundary         \n",
    "    facets = dolfinx.mesh.locate_entities_boundary(mesh, 1, lambda x : x[1] > 0)\n",
    "    dofs_outer = dolfinx.fem.locate_dofs_topological(V0, 1, facets)\n",
    "    bc_out = dolfinx.DirichletBC(zero, dofs_outer)\n",
    "    \n",
    "    # bcs or theta\n",
    "    bcs = [bc_out, bc_inner]\n",
    "    \n",
    "    # Define variational problem. We solve a simple laplacian\n",
    "    theta, theta_ = ufl.TrialFunction(V0), ufl.TestFunction(V0)\n",
    "    \n",
    "    a = ufl.dot(ufl.grad(theta), ufl.grad(theta_)) * ufl.dx\n",
    "    L = zero * theta_ * ufl.dx(domain=mesh) \n",
    "    \n",
    "    problem = dolfinx.fem.LinearProblem(a, L, bcs=bcs, petsc_options={\"ksp_type\": \"preonly\", \"pc_type\": \"lu\"})\n",
    "    thetah = problem.solve()\n",
    "    return thetah\n",
    "\n",
    "uh, energy = elastic_solver.solve_elasticity(Lx=1,Ly=.5,Lcrack=.3,lc=.05,refinement_ratio=10,dist_min=.2,dist_max=.3)\n",
    "plt.figure()\n",
    "plt.colorbar(dolfinx.plotting.plot(uh,mode=\"displacement\"),orientation=\"horizontal\")\n",
    "plt.title(\"Solution of the elastic problem\")\n",
    "\n",
    "plt.figure()\n",
    "thetah = create_theta_field(uh.function_space.mesh,Lcrack=.3,R_int=.1)\n",
    "plt.colorbar(dolfinx.plotting.plot(thetah,vmin=0,vmax=1),orientation=\"horizontal\")\n",
    "plt.title(r\"$\\theta$-field\");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "promotional-explanation",
   "metadata": {},
   "source": [
    "From teh scalr field, we define a vector field by multiplying by the vector [1,0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "existing-officer",
   "metadata": {},
   "outputs": [],
   "source": [
    "thetah_ = dolfinx.Function(uh.function_space)\n",
    "project(thetah * ufl.as_vector([1,0]),thetah_)\n",
    "dolfinx.plotting.plot(thetah_,scale=10, title=r'Vector valued $\\theta$-field')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "least-tanzania",
   "metadata": {},
   "source": [
    "Hence, we can compute the ERR with the formula\n",
    "\n",
    "$$\n",
    "G  = \\int_\\Omega \\left(\\sigma(\\varepsilon(u))\\cdot(\\nabla u\\nabla\\theta)-\\dfrac{1}{2}\\sigma(\\varepsilon(u))\\cdot \\varepsilon(u) \\mathrm{div}(\\theta)\\,\\right)\\mathrm{dx}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "coordinated-moisture",
   "metadata": {},
   "outputs": [],
   "source": [
    "epsh_ = elastic_solver.eps(uh)\n",
    "sigh_ = elastic_solver.sigma(epsh_)\n",
    "\n",
    "first_term = ufl.inner(sigh_,ufl.grad(uh) * ufl.grad(thetah_)) * ufl.dx\n",
    "second_term = - 0.5 * ufl.inner(sigh_,epsh_) * ufl.div(thetah_) * ufl.dx\n",
    "\n",
    "G_theta = 2 * dolfinx.fem.assemble_scalar(first_term + second_term)\n",
    "print(f'The ERR computed with the G-theta method is {G_theta:2.4f}' )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "executive-benefit",
   "metadata": {},
   "source": [
    "Interesting, as alternative, we can let `UFL` compute the symbolic derivative, and obtain the same result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ordered-imagination",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the energy with the change of variables\n",
    "theta_ = dolfinx.Function(uh.function_space)\n",
    "F = ufl.Identity(2) + ufl.grad(theta_)  # Gradient of transformation\n",
    "eps_ = ufl.sym( ufl.grad(uh) * ufl.inv(F) )\n",
    "P = 0.5 * ufl.inner(elastic_solver.sigma(eps_), eps_) * ufl.det(F) * ufl.dx\n",
    "\n",
    "# Leaverage UFL to compute the symbolic derivative  \n",
    "P_theta = ufl.derivative(P, theta_, thetah_)\n",
    "G_theta_ufl = dolfinx.fem.assemble_scalar(-2*P_theta)\n",
    "print(f'The ERR computed with the G-theta method is {G_theta_ufl:2.4f}' )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "industrial-edmonton",
   "metadata": {},
   "outputs": [],
   "source": [
    "Ls = np.linspace(Lcrack*.7,Lcrack*1.3,10)\n",
    "energies = np.zeros_like(Ls)\n",
    "Gs = np.zeros_like(Ls)\n",
    "for (i, L) in enumerate(Ls):\n",
    "    uh, energies[i] = elastic_solver.solve_elasticity(Lx=1,Ly=.5,Lcrack=L,lc=.1,refinement_ratio=10,dist_min=.2,dist_max=.3)\n",
    "    thetah = create_theta_field(uh.function_space.mesh,Lcrack=L,R_int=.1)\n",
    "    thetah_ = thetah*ufl.as_vector([1,0])\n",
    "    epsh_ = elastic_solver.eps(uh)\n",
    "    sigh_ = elastic_solver.sigma(epsh_)\n",
    "    first_term = ufl.inner(sigh_,ufl.grad(uh)*ufl.grad(thetah_))*ufl.dx\n",
    "    second_term = - 0.5 * ufl.inner(sigh_,epsh_)*ufl.div(thetah_)*ufl.dx\n",
    "    Gs[i] = 2*dolfinx.fem.assemble_scalar(first_term + second_term)\n",
    "    # Save solution in XDMF format\n",
    "    with dolfinx.io.XDMFFile(MPI.COMM_WORLD, \"output/gtheta.xdmf\", \"w\") as file:\n",
    "        file.write_mesh(mesh)\n",
    "        file.write_function(uh, L)        \n",
    "        file.write_function(thetah, L)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "colonial-navigation",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(Ls[0:-1],ERR_naif,\"o\",label=r\"Finite difference\")\n",
    "ax.plot(Ls,Gs,\"*\",label=r\"$G_\\theta$\")\n",
    "plt.xlabel(\"L_crack\")\n",
    "plt.ylabel(\"G\")\n",
    "plt.legend()\n",
    "plt.axhline(G_estimate,linestyle='--',color=\"gray\")\n",
    "plt.axvline(Lcrack,linestyle='--',color=\"gray\") "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
