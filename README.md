[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.kitware.com%2Fcmaurini%2Fnewfrac-core-numerics.git/HEAD)

# Basic computational methods for fracture mechanics

This repository contains the material for the 4-hour lectures on the basics of computational methods for fracture (21/01/2021), as taught during the NEWFRAC Core School 2021.

## Overview of the content of this repository

 - Short introductory lecture notes in the pdf [Core_School_numerical_NOTES.pdf](Core_School_numerical_NOTES.pdf)
  
 -  Two numerical examples in the form of Jupyter notebooks based on fenics-x in the directory :
 
       -  [01-LinearElasticity.ipynb](01-LinearElasticity.ipynb) which introducies the solution of a linear elastic problem using fenics-x
   
       - [02-LEFM.ipynb](02-LEFM.ipynb) which gives a short demo of the basic methods for computing the stress-intensity factor and the energy release rate.

The repository includes also few python files that we will be imported in the notebook (meshes.py, utils.py, [elastic_solver.py](elastic_solver.py))

## How to run the notebooks

To use dolfin-x and run these examples on a jupyter notebook on your browser you can follow the following instructions.

### Self hoster Docker container 

  1. Install [Docker-desktop](https://www.docker.com/products/docker-desktop) on your computer.

  2. Open a terminal in directory where you would like to work and download this repository by executing the command:
      ```console
      git clone https://gitlab.kitware.com/cmaurini/newfrac-core-numerics.git 
      ```
      Then, go inside the new directory:
      ```console
      cd newfrac-core-numerics
      ```
      If you do not have `git` installed, either install it (suggested) or simply download the zip archive [here](https://gitlab.kitware.com/cmaurini/newfrac-core-numerics/-/archive/master/newfrac-core-numerics-master.zip)

  3. Run the container by executing the docker command. This will share also the current working directory with the directory `/root/shared dolfinx/lab` in the container.
      ```console
      docker run --init -p 8888:8888  --name dolfinx-newfrac -v "$(pwd)":/root/shared dolfinx/lab
      ```
      If you are on windows you may need to replace `"$(pwd)"` with `"%cd%"`, or the explicit path of the folder you want to share.
Once the command is executed (ths can take some time the first time, as you need to download the container), open in your favority browser the address provided in the terminal starting with `http://127.0.0.1:8888`. This will give you a working jupyter-lab environment. You will work in the directory `/root/shared` of the virtual machine, which will be shared with the directory where you executed the command in your terminal.
   
### Remotely hosted Docker container on binder

If you have any problem with the above solution, you can have a direct access to a working interactive online version of the notebooks clicking on this badge:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.kitware.com%2Fcmaurini%2Fnewfrac-core-numerics.git/HEAD)

This is quick and easy to play with the notebooks during the class. But you need to be online and you do not have the content shared on your computer.

## Content of lesson I (theory)

We introduce the basics of computational methods for linear elastic fracture mechanics (LEFM), see the [pdf notes](Core_School_numerical_NOTES.pdf):
- Methods for computation of the stress intensity factor(s)
  - Extrapolation of finite element solution
  - Special finite elements at the crack tip
- Methods for computation of the energy release rate
  - Virtual crack extension: the finite difference approximation
  - Virtual crack extension: the G-θ method

## Content of lesson II (numerical tutorial)


We propose a hand-on session providing interactive examples with a practical implementation of the concepts and methods introduced in lesson I. 

The examples will be based on interactive Jupyter notebooks in python language. They are based on the use of the finite element library [fenics-x](https://github.com/FEniCS/dolfinx).  

*0. Short overview of the [fenics-project](https://fenicsproject.org), see the [fenics tutorial](https://fenicsproject.org/tutorial/) and the new [fenics-x tutorial](https://jorgensd.github.io/dolfinx-tutorial/fem.html)*

*1. How to solve linear elasticity including a crack as a geometric feature  [(01-LinearElasticity.ipynb)](01-LinearElasticity.ipynb)* 

*2. How to calculate the stress intensity facture (SIF) and the energy release rate (ERR) [(02-LEFM.ipynb)](02-LEFM.ipynb) :*
   - a. Extrapolation of finite element solution
   - b. Virtual crack extension: the finite difference approximation
   - c. Virtual crack extension: the G-θ method
    

## Possible ideas for student projects

We encourage the Ph.D. students enrolled in the Newfrac network to work on a personal project on the basis of these lecture notes. You will find below some ideas. You can submit to us the results in the form of a short (2-pages) pdf report and, ideally, a repository in gitlab with your own codes. You can [fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) the present repository for this purpose. 

P1. Perform the same computation on a full domain (no symmetry, modify the mesh)

P2. Study the effect of the mesh and other numerical parameters

P3. Study the case of a hard device (imposed displacement)

P4. Study the case with thermal loading (e.g. a slab with thermal shock loading)

P5. Calculate the deritivate of the energy release rate with the $G-\theta$ method  and compare it to the finite-difference approximation

P6. For the sake of sipmlicity, all the present codes are written for working on a single processor. Few changes (few calls to `MPI` functions) are necessary to have them working in parallel. Make a parallel version of a linear elastic solver with the ERR computation with the $G-$\theta$ version and test it on multiple processors. Refer to the [fenics-x tutorial](https://jorgensd.github.io/dolfinx-tutorial/fem.html) and the [fenics-x demos](https://docs.fenicsproject.org/dolfinx/master/python/demos.html) for this, which all written to be executed in parallel.

## Acknowledgements

The funding received from the European Union’s Horizon 2020 research and innovation programme under Marie Skłodowska-Curie grant agreement No. 861061-NEWFRAC is gratefully acknowledged.
