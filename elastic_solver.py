import dolfinx
import ufl

import numpy as  np

from meshes import generate_mesh_with_crack

E = 1.
nu = 0.3

mu = E / (2.0 * (1.0 + nu))
lmbda = E * nu / ((1.0 + nu) * (1.0 - 2.0 * nu))

# Plane stress
lmbda = 2*mu*lmbda/(lmbda+2*mu)

def eps(u):
    return ufl.sym(ufl.grad(u))

def sigma(eps):
    return 2.0 * mu * eps + lmbda * ufl.tr(eps) * ufl.Identity(2)
    
def solve_elasticity(Lx=1,Ly=.5,Lcrack=.3,lc=.1,refinement_ratio=10,dist_min=.2,dist_max=.3):
    
    mesh = generate_mesh_with_crack(Lcrack=Lcrack,
                         Lx=Lx,
                         Ly=Ly,
                         lc=lc,
                         refinement_ratio=refinement_ratio,
                         dist_min=dist_min,
                         dist_max=dist_max)


    # Volume force
    b = dolfinx.Constant(mesh,ufl.as_vector((0,0)))
    
    # surface force on the top
    f = dolfinx.Constant(mesh,ufl.as_vector((0,0.1)))
    
    # Finite element function space (Linear lagrange triangle)
    element = ufl.VectorElement('Lagrange',mesh.ufl_cell(),degree=1,dim=2)
    V = dolfinx.FunctionSpace(mesh, element)
    
    # Define set to apply the boudnary conditions
    def bottom_no_crack(x):
        return np.logical_and(np.isclose(x[1], 0.0), x[0] > Lcrack)
    
    def right(x):
        return np.isclose(x[0], Lx)
    
    def top(x):
        return np.isclose(x[1], Ly)
    
    # Define Dirichlet BCs (this quite complex in dolfin-x, you can skip at the beginning)
    V_0 = V.sub(0).collapse()
    zero = dolfinx.Function(V_0)
    with zero.vector.localForm() as bc_local:
        bc_local.set(0.0)
    
    blocked_dofs_bottom = dolfinx.fem.locate_dofs_geometrical((V.sub(1), V.sub(1).collapse()), bottom_no_crack)
    blocked_dofs_right = dolfinx.fem.locate_dofs_geometrical((V.sub(0), V.sub(0).collapse()), right)
    bc0 = dolfinx.DirichletBC(zero, blocked_dofs_bottom, V.sub(1))
    bc1 = dolfinx.DirichletBC(zero, blocked_dofs_right, V.sub(0))
    bcs = [bc0,bc1]
    
    # Define the bulk and surface mesure. 
    # In this example the surface measure ds includes tags to apply Neumann bcs
    dx = ufl.Measure("dx",domain=mesh)
    top_facets = dolfinx.mesh.locate_entities_boundary(mesh, 1, top)
    mt = dolfinx.mesh.MeshTags(mesh, 1, top_facets, 1)
    ds = ufl.Measure("ds", subdomain_data=mt)
    
    # Define the variational problem
    u = ufl.TrialFunction(V)
    v = ufl.TestFunction(V)
    
    def a(u,v):
        """The bilinear form of the weak formulation"""
        return ufl.inner(sigma(eps(u)), eps(v)) * dx
    
    def L(v): 
        """The linear form of the weak formulation"""
        return ufl.dot(b, v) * dx + ufl.dot(f, v) * ds(1)
    
    # We solve using a direct solver
    problem = dolfinx.fem.LinearProblem(a(u,v), L(v), bcs=bcs, petsc_options={"ksp_type": "preonly", "pc_type": "lu"})
    uh = problem.solve()
    uh.name = "displacement"
    
    energy = dolfinx.fem.assemble_scalar(0.5 * a(uh, uh) - L(uh))
    print(f"Crack length: {Lcrack:2.3e} - Potential energy: {energy:2.3e}")
    
    return uh, energy



if __name__ == '__main__':
    
    import matplotlib.pyplot as plt
    from mpi4py import MPI
    from pathlib import Path
    
    uh, energy = solve_elasticity(Lx=1,Ly=.5,Lcrack=.3,lc=.05,refinement_ratio=10,dist_min=.2,dist_max=.3)

    Path("output").mkdir(parents=True, exist_ok=True)    
    
    with dolfinx.io.XDMFFile(MPI.COMM_WORLD, "output/elasticity-demo.xdmf", "w") as file:
        file.write_mesh(uh.function_space.mesh)
        file.write_function(uh)
    
    plt.colorbar(dolfinx.plotting.plot(uh,mode="displacement"),orientation="horizontal")
    plt.title('Elastic solver - displacement field')
    plt.savefig("output/elasticity-demo.png")
